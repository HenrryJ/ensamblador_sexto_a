;Estudiante:Henrry Jimenez
;Fecha: 05/08/2020

%macro imprimir 2
	mov al, 4
	mov bl, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
%macro lectura 2
       mov eax,3
       mov ebx,0
       mov ecx,%1
       mov edx,%2
       int 80H
%endmacro
section .data
	msj1 db '*'
	len_msj1  equ $-msj1 
	msj2 db '='
	len_msj2  equ $-msj2

	a_res db 'Ingrese el primer valor',10
	len_a_res equ $-a_res

	nueva_linea db 10, ''
	len_nueva_linea  equ $-nueva_linea

	new_line db "",10
	len_new_line equ $-new_line

	
	
section .bss
	a resb 2
	b resb 2
	c resb 2
section .txt
	global _start

_start:
	

	;imprimir a_res, len_a_res	
	;lectura a, 1

	
	call uno			
ciclo:
	
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo
;***************************************
s_enter:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;************************************
call dos

ciclo2:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo2
;********************************************
s_enter2:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call tres

ciclo3:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo3
;********************************************
s_enter3:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call cuatro

ciclo4:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo4
;********************************************
;********************************************
s_enter4:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call cinco

ciclo5:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo5
;********************************************
s_enter5:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call seis

ciclo6:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo6
;********************************************
s_enter6:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call siete

ciclo7:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo7
;********************************************
s_enter8:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call ocho

ciclo8:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo8
;********************************************
s_enter9:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;******************************************
call nueve

ciclo9:
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	call imprimi
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo9
;********************************************

;*********Ingreso de la serie de número[1-9]**************

uno:		
	mov al, 1
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
dos:		
	mov al, 2
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
tres:		
	mov al, 3
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
cuatro:		
	mov al, 4
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
cinco:		
	mov al, 5
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
seis:		
	mov al, 6
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
siete:		
	mov al, 7
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
ocho:		
	mov al, 8
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
nueve:		
	mov al, 9
	;sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
	ret
;*********Impresiones con call*************************
imprimir_uno:	
	mov eax, 4
	mov ebx, 1
	mov ecx, a
	mov edx, 1
	int 80h
	ret
imprimir_dos:	
	mov eax, 4
	mov ebx, 1
	mov ecx, b
	mov edx, 1
	int 80h
	ret
imprimir_tres:	
	mov eax, 4
	mov ebx, 1
	mov ecx, c
	mov edx, 1
	int 80h
	ret
;*************************************************
imprimi:
	call imprimir_uno
	imprimir msj1, len_msj1
	call imprimir_dos
	imprimir msj2, len_msj2
	call imprimir_tres
	imprimir nueva_linea, len_nueva_linea
	ret
 

	mov eax, 1
	int 80h
	
