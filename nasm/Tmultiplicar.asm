;Estudiante:Henrry Jimenez
;Fecha: 05/08/2020
%macro imprimir 2
	mov al, 4
	mov bl, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
%macro lectura 2
       mov eax,3
       mov ebx,0
       mov ecx,%1
       mov edx,%2
       int 80H
%endmacro
section .data
	msj1 db '*'
	len_msj1  equ $-msj1 
	msj2 db '='
	len_msj2  equ $-msj2

	a_res db 'Ingrese el primer valor',10
	len_a_res equ $-a_res

	nueva_linea db 10, ''
	len_nueva_linea  equ $-nueva_linea

	new_line db "",10
	len_new_line equ $-new_line

	
	
section .bss
	a resb 2
	b resb 2
	c resb 2
section .txt
	global _start

_start:
	

	imprimir a_res, len_a_res	
	lectura a, 1

	mov al, [a]
	sub al, '0'
	add al, '0'
	mov[a], al
	mov cx, 1
				
ciclo:
	
	push cx
	mov ax, [a]
	sub ax, '0'
	mul cx
	add ax, '0'
	mov [c], ax
	add cx, '0'
	mov [b], cx
	imprimir a, 1
	imprimir msj1, len_msj1
	imprimir b, 1
	imprimir msj2, len_msj2
	imprimir c, 1
	imprimir nueva_linea, len_nueva_linea
	pop cx
	inc cx
	cmp cx,10
	jnz ciclo
;***************************************
s_enter:
	mov eax, 4
	mov ebx,1
	mov ecx, new_line
	mov edx, len_new_line
	int 80h	
	
;************************************
	

	mov eax, 1
	int 80h
