;Suma de dos números ingresados por teclado
;Estudiante: Henrry Manuel Jiménez Calva
section .data
	num1 db 'Ingrese número1 :'
	len_num1 equ $-num1

	num2 db 'Ingrese número2 :'
	len_num2 equ $-num2

	resultado db 'El resultado es:',10
	len_resultado equ $-resultado

section .bss
	suma resb 10
	n1 resb 5
	n2 resb 5 

section .text
	global_start
_start:
;************Imprimir el mensaje 1****************************
	mov eax, 4       
	mov ebx, 1       
	mov ecx, num1     
	mov edx, len_num1       
	int 80h	
;*************Lectura de número 1************************************************
	mov eax, 3    ;Operación lectura   
	mov ebx, 2    ;Estandar de entrada       
	mov ecx, n1   ;Se esta capturando por teclado 
	mov edx, 5    ;Número de caracteres reservados   
	int 80h	   

;********************Imprimir mensaje 2*******************************************
	;leer el segundo valor
	mov eax, 4
	mov ebx, 1
	mov ecx, num2
	mov edx, len_num2
	int 80h
;*********************lectura de número 2**************************************
	mov eax, 3
	mov ebx, 1
	mov ecx, n2
	mov edx, 5
	int 80h
;**********************Imprimir resultado************************************
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, len_resultado
	int 80h
;************************Asignar valor a variable suma***********************************
	mov eax, [n1]
	mov ebx, [n2]
	sub eax, '0'
	sub ebx, '0'
	add eax, ebx
	add eax, '0'
       
        mov [suma], eax
;****************************Imprimir el mensaje suma**********************************
	mov eax, 4
	mov ebx, 1
	mov ecx, suma
	mov edx, 5
	int 80h
;***************************************salir del sistema
	mov eax, 1
	int 80h
