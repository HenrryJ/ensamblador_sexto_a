;Estudiante: Henrry Jiménez
section .data
	msg db "El resultado es:", 10
	len equ $- msg 

section .bss
	res resb 1
	resax resb 2


section	.text
   global _start    

_start:             
	mov al,'3' 	
	sub al, '0'
	
	mov bl, '1'	
	sub bl, '0'

	mul bl		
	add al, '0'
	
	mov [res], al
	mov [resax], ax

	mov ecx, msg	
	mov edx, len
	mov eax, 4	
	mov ebx, 1	
	int 80h	
	
	mov ecx,res
	mov edx, 1
	mov eax, 4	
	mov ebx, 1
	int 80h

	mov ecx,resax
	mov edx, 1
	mov eax, 4	
	mov ebx, 1
	int 80h	

	mov eax,1 
	int 80h

