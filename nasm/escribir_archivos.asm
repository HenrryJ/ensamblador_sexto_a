%macro escribir 2
	mov eax, 4
    	mov ebx, 1
    	mov ecx, %1
    	mov edx, %2
    	int 80h
%endmacro

section .data
	mjs1 db "Ingresa datos en el archivo", 10
	len_mjs1 equ $-mjs1

	archivo db "/home/henrry/Escritorio/sexto ensamblador/archivo.txt"

section .bss
	texto resb 30
	id_archivo resd 1
	

section .text
	global_start


leer:
	mov eax, 3
	mov ebx, 0
	mov ecx, texto
	mov edx, 10;
	int 80h
	ret


_start:



	mov eax, 8 ;Definimos el servicio para crear acrhivo, trabajar con archivos
	mov ebx, archivo   ;definimos la direccion del archivo 
	mov ecx, 1  ; modo de acceso
		     ; O-RDONLY 0: el archivo se abre solo para leer
		     ; O-RDONLY 1: el archivo se abre solo para escritura
		     ; O-RDWR 2: el archivo se abre solo para leer y escribir
		     ; O-CREATE 256: Crea el archivo en caso que no exista
		     ; O-APPEND 2000h: El archivo se abre solo para escrituta final
	mov edx, 777h
	int 80h

	test eax, eax


	
	jz salir     ; se ejecuta cuando existen errores en el archivo
        
	mov dword[id_archivo], eax	
	escribir mjs1, len_mjs1

	
	
	call leer
	
	;*********escritura en archivo************
	mov eax, 4
    	mov ebx, [id_archivo]
    	mov ecx, texto
    	mov edx, 20
    	int 80h
salir: 
	mov eax, 1
	int 80h

