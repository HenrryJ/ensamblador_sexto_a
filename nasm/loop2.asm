;Henrry Manuel Jimenez Calva
;Fecha: 29/07/2020
%macro impirmir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro

section .data
    msg db '*',10
    salto db 10,' '

section .text
    global _start
	mov ecx, 9
_start:

    mov ebx, 9					
    mov ecx, 9					
             
principal:
	push rbx						
	cmp ebx, 0					
	jz salir					
	jmp asterisco 				

asterisco:
	dec ecx						
	push rcx
	impirmir msg				
	pop rcx
	cmp ecx, 0
	jg asterisco				
	impirmir salto
	pop rbx
	dec ebx
	mov ecx,9					
	loop principal

salir:
    mov eax, 1
    int 80h

