;Estudiante: Henrry Jiménez
%macro impirmir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro

section .data
    msg db '*',10
    salto db 10,' '

section .text
    global _start
_start:

    mov bx, 9					
    mov cx, 9					
             
principal:
	push cx						
	cmp cx, 0					
	jz salir					
	;jmp asterisco 				

asterisco:
	dec cx						
	push cx
	impirmir msg				
	pop cx
	cmp cx, 0
	jg asterisco				
	impirmir salto
	pop bx
	dec bx
	mov cx,bx					
	;jmp principal

salir:
    mov eax, 1
    int 80h

