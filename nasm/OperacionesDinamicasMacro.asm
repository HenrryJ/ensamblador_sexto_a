;Estudiante:Henrry Manuel Jiménez
;Fecha:22/06/2020
;Operaciones
%macro imprimir 2
	mov al, 4
	mov bl, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
%macro lectura 2;----------->Lectura con macro
       mov eax,3
       mov ebx,0
       mov ecx,%1
       mov edx,%2
       int 80H
%endmacro

section .data
	a_res db 'Ingrese el primer valor',10
	len_a_res equ $-a_res

	b_res db 'Ingrese el segundo valor',10
	len_b_res equ $-b_res

	suma_resultado db 'El resultado de la suma es:',10
	len_suma_resultado equ $-suma_resultado
	
	resta_resultado db 'El resultado de la resta es:',10
	len_resta_resultado equ $-resta_resultado

	mul_resultado db 'El resultado de la multiplicacion es:',10
	len_mul_resultado equ $-mul_resultado
	
	cociente_resultado db 'El resultado de la cociente es:',10
	len_cociente_resultado equ $-cociente_resultado
	
	residuo_resultado db 'El resultado de la residuo es:',10
	len_residuo_resultado equ $-residuo_resultado
	
section .bss
        a resb 1
        b resb 1
	suma resb 1
	resta resb 1
	multi resb 1
	cociente resb 1
	residuo resb 1

section .text
	global_start
_start:

;***************Lectura*************
	imprimir a_res, len_a_res
	lectura a, 2
	imprimir b_res, len_b_res
	lectura b, 2
;***************ResultadoSuma*************
	imprimir suma_resultado, len_suma_resultado

	mov al, [a]; el valor es b
	sub al, '0'; Convertir cadena a enteros
	mov bl, [b]
	sub al, '0'

	add al, bl
	add al, '0'
	
	mov [suma], al
	imprimir suma,1

;***************ResultadoResta*************
    	imprimir resta_resultado, len_resta_resultado
	mov al, [a]
	sub al, '0'
	mov bl, [b]
	sub bl, '0'
	sub al, bl
	add al, '0'
	mov [resta], al
	imprimir resta,1

;***************Resultado multipl*************
       imprimir mul_resultado, len_mul_resultado
        mov al, [a]
        sub al,'0'
	mov bl, [b]
	sub bl, '0'
	mul bl, 
	add al, '0'
	mov[multi], al
	imprimir multi,1
	
;***************Resultado cociente y residuo*************
    	imprimir cociente_resultado, len_cociente_resultado
	mov al, [a]
	sub al, '0'

	mov bl, [b]
	sub bl, '0'

	div bl
	add al, '0'
	add ah, '0'
	
	mov [cociente], al
	mov [residuo], ah

	imprimir cociente, 1
	imprimir residuo_resultado, len_residuo_resultado
	imprimir residuo, 1
	 
;***************Fin*************
    	mov eax, 1
	int 80h
