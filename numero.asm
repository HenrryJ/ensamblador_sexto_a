;Estudiante: Henrry Manuel Jiménez Calva
section .data
	mensaje db 'Ingrese un número', 10
	len_mensaje equ $-mensaje
	mensaje_presentacion db 'El número ingresado es',10
	len_mensaje_presentacion  equ $-mensaje_presentacion 
section .bss
	numero resb 5

section .text
	global_start
_start:
	;********************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	int 80h
	;********************
	mov eax, 3       ;Define el tipo de operación
	mov ebx, 2       ;estandar de entrada
	mov ecx, numero  ; lo que captura en teclado
	mov edx, 5       ; número de carateres
	int 80h		 ;interrupción de gnu linux
;**********************Imprime mensaje presentacion*****************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje_presentacion
	mov edx, len_mensaje_presentacion
	int 80h
;*************************Imprime numero**********************************
	mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 5
	int 80h
;********************************************************salir del sistema
	mov eax, 4
	int 80h

