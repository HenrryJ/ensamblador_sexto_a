;Comentar el significado de cada línea
;Estudiante: Henrry Manuel Jiménez Calva

%macro imprimir 2;------------------>Realiza un metodo llamado imprimir,recibe 2 parametros, permitiendo presentar el mensaje , mayor de seguridad 
	mov eax,4;------------------>Realiza un movimiento directo al registro eax, se mueve 4 decimal al registro eax, esta escribiendo
	mov ebx,1;------------------>Le enviamos la dirección en donde esta nuestro archivo a imprimir %1, indica la salida
	mov ecx,%1;------------------->Realiza un movimiento al registro, dirección en donde esta nuestro archivo %1, esta refiriendo a memoria 
	mov edx,%2;------------------->Me indica la longitud de la acción(imprimir) en este caso 2, indicando que va ha recibir algo.
	int 80H;;---------------------->Interrupción de Sistema Operativo(linux), envía una señal al S.O ejecutando la subrutina.	
%endmacro;-------------------------->Indica el fin del %macro, fin del método imprimir.

%macro leer 2;---------------------->Realiza un método llamado leer,recibe dos parámetros, permitiendo leer el mensaje , mayor de seguridad
    mov eax,3;----------------------->Realiza un movimiento directo al registro eax, se mueve 3 decimal al registro eax.
    mov ebx,0;----------------------->Envía la dirección en donde esta nuestro archivo a leer %1, indicando la tarea que tiene que realizar, indicando la salida(por teclado).
    mov ecx,%1;---------------------->Realiza un movimiento al registro, dirección en donde esta nuestro archivo a leer %1, direccion de memoria. 
    mov edx,%2;---------------------->Me indica el la longitud de la acción(Leer) en este caso 2.
    int 80H;------------------------->Interrupción de Sistema Operativo(linux), ejecuta la acción. 
%endmacro;--------------------------->Indica el fin del %macro, fin del método leer.


; ecx,modo de acceso
; edx, permisos
section .bss;------------------------------------------------>Definición de variables
	auxiliar resb 30;------------------------------------>Primer variable  con la respectiva cantidad de espacio, espacio de memoria de tamaño 30
	auxiliarb resb 30;----------------------------------->Segunda variable con la respectiva cantidad de espacio, espacio de memoria de tamaño 30
	auxiliarc resb 30;----------------------------------->Tercera variable con la respectiva cantidad de espacio, espacio de memoria de tamaño 30


section .data;-------------------------------------------------------->Declaración de datos constantes
	msg db 0x1b ,"       " ; 6 espacios para contener al dato
	lenmsg equ $-msg;------------------------------------------->--Longitud del dato o cadena 



	salto db " ",10;----------------------------------------->Instrucción de salto de línea, definiendo el tipo de variable
	lensalto equ $-salto;------------------------------------>Longitud de la cadena




section .text;-------------------------------->Inicia la seccion del código o instrucciones.
    global _start;---------------------------------->Acceder al archivo o declarar inicio.    

_start:
	
	mov ecx,9;--------------------------------->Movimiento al registro ecx el valor 9.

	mov al,0;------------------------------------->Mover el dato al registro de 8 el numero 0
	mov [auxiliar],al;---------------------------->Se esta moviendo el dato del registro a la variable auxiliar.

cicloI:;----------------------------------------------->Este es un bloque de código que contiene a más métodos o bloques.
	push ecx;-------------------------------------->Esta guardando una pila.
	mov ecx,9;------------------------------------->Esta moviendo el valor 9 al registro ecx.

	mov al,0;-------------------------------------->Esta moviendo el valor 0 al registro al.
	mov [auxiliarb],al;---------------------------->Esta moviendodo del registro a la variable auxiliar

	cicloJ:;---------------------------------------->Este es una función que indica el inicio del bloque del código.
		push ecx;------------------------------->Esta guardando una pila.


		call imprimir0al9;---------------------->Esta llamando al método imprimir0a19.
		

	;	imprimir msg2,lenmsg2;------------------>Esta recibiendo los parametros a recebir, para iniciar la ejecución.

	fincicloJ:;-------------------------------------->Función o etiqueta
		mov al,[auxiliarb];---------------------->Esta enviando el valor de la variable al registro
		inc al;----------------------------------->Se esta presentado un incremento
		mov [auxiliarb],al;----------------------->Movimiento del valor de registro a la variable auxiliar.

		pop ecx;--------------------------------->Saliedo de la pila.
		loop cicloJ;----------------------------->Recorriendo el cicloJ.
		
	;imprimir salto,lensalto;

fincicloI:;--------------------------------------------->Método
	mov al,[auxiliar];------------------------------>Mover el auxiliar al registro al.
	inc al;----------------------------------------->incrementar el registro al.
	mov [auxiliar],al;------------------------------>Mover el registro al a la variable auxiliar.

	pop ecx;----------------------------------------->Cerrar o salir de pila.
	loop cicloI;------------------------------------->Recorrer el cicloI.
	

salir:
	mov eax, 1;--------------------------------------->Movimiento directo de 1 al registro eax.
	int 80H;------------------------------------------>Interrupción al sistema operativo, para realizar la ejecución.



imprimir0al9:;------------------------------------------->Método
	
	mov ebx,"[";------------------------------------>Esta agregando un corchete a la variable ebx.
	mov [msg+1], ebx;------------------------------->Esta agregando el valor de la constante al registro en la primera posición.

	mov bl,[auxiliar]------------------------------>Esta moviendo el valor de auxiliar en el registro b1.
	add bl,'0';------------------------------------->Esta aggregando el valor de cero en cadena a la variable b1.
	mov [msg+2], bl;-------------------------------->Esta moviendo el valor de la variable en el registro, se esta asigando en la posicion 2.


	mov ebx,";";------------------------------------>Se esta moviendo el ";"  al registro ebx
	mov [msg+3], ebx;------------------------------->Se esta moviendo el registro en la constante en la posicion 3

	
	mov bl,[auxiliarb];---------------------------->Se esta moviendo la referencia de la variable auxiliar al registro b1. 
	add bl,'0';------------------------------------>Esta agregando una cadena
	mov [msg+4],bl;-------------------------------->En la posición 4, se asgigna el valor de b1. 

	mov ebx,"fJ";---------------------------------->Esta agregando una cadena al registro ebx
	mov [msg+5], ebx;------------------------------>Esta agragando en la posicion 5 el registro ebx.

	imprimir msg,lenmsg;--------------------------->Envio de parametros

	ret;------------------------------------------->Se esta haciendo un retorno del método.
